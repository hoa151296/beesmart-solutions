<?php

namespace Modules\Product\Transformers\Admin;

use Illuminate\Http\Resources\Json\Resource;

class ProductTransformer extends Resource
{
    public function toArray($request)
    {
        $image = !empty($this->getImage()) ? $this->getImage() : '';
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'price' => (int)$this->price,
            'descriptions' => strip_tags($this->descriptions),
            'image' => $image,
            'urls' => [
                'delete_url' => route('api.product.products.destroy', $this->id),
            ],
        ];

        return $data;
    }
}
