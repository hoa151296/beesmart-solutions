import Producttable from './components/Producttable.vue';
import ProductForm from './components/ProductForm.vue';


const locales = window.AsgardCMS.locales;

export default [
    {
        path: '/product/products',
        name: 'admin.product.products.index',
        component: Producttable,
        props: {
            locales,
            pageTitle: 'List product',
        },
    },
    {
        path: '/product/products/create',
        name: 'admin.product.products.create',
        component: ProductForm,
        props: {
            locales,
            pageTitle: 'New product',
        },
    },
    {
        path: '/product/products/:productId/edit',
        name: 'admin.product.products.edit',
        component: ProductForm,
        props: {
            locales,
            pageTitle: 'Edit product',
        },
    },
];
