<div class="box-body">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name', trans('product::products.form.name')) !!}<span class="required" style="color: red;" aria-required="true"> * </span>
                {!! Form::text('name', old('name'), ['class' => 'form-control','data-slug' =>  'source', 'id' => 'name', 'placeholder' => trans('product::products.form.name')]) !!}
                {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                {!! Form::label('price', trans('product::products.form.price')) !!}<span class="required" style="color: red;" aria-required="true"> * </span>
                {!! Form::number('price', old('price'), ['class' => 'form-control','data-slug' =>  'source', 'id' => 'price', 'placeholder' => trans('product::products.form.price')]) !!}
                {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<div class="form-group{{ $errors->has('descriptions') ? ' has-error' : '' }}">
	    		@editor('descriptions',trans('product::products.form.descriptions'),old('descriptions'))
	    	</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                <label class="ccontrol-label ">{{ trans('product::products.form.image') }} </label><span class="required" style="color: red;" aria-required="true"> * </span>
                <div class="multi-upload-file multi-upload-file3">
                    <input type="file" style="display: none" id="file-upload3" class="input-upload-file" accept="image/x-png,image/gif,image/jpeg"/>
                    <input id="image" name="image" value="{{ old('image') }}" type='hidden'/>

                    <div class="image-preview"></div>
                    <label class="mark-input" for="file-upload3"><span>+</span></label>
                    <div style="clear:both"></div>
                </div>
                {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                <div class="show-error-message-image" style="display: none;"><span style="color: red;">{{ trans('product::products.validation.image required') }}</span></div>
            </div>
    	</div>
    </div>
</div>