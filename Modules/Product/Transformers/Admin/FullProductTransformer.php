<?php

namespace Modules\Product\Transformers\Admin;

use Illuminate\Http\Resources\Json\Resource;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class FullProductTransformer extends Resource
{
    public function toArray($request)
    {
        $image = !empty($this->getImage()) ? $this->getImage() : '';
        
        $pageData = [
            'id' => $this->id,
            'name' => $this->name,
            'price' => (int)$this->price,
            'descriptions' => $this->descriptions,
            'image' => $image,
        ];


        return $pageData;
    }
}
