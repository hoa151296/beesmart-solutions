<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\ProductRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Product\Entities\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Product\Events\ProductWasCreated;
use Modules\Product\Events\ProductWasUpdated;


class EloquentProductRepository extends EloquentBaseRepository implements ProductRepository
{
	/**
     * Paginating, ordering and searching through pages for server side index table
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor($request,$perPage): LengthAwarePaginator
    {

        $model = $this->allWithBuilder();

        if ($request->get('search') !== null) {
            $term = $request->get('search');
            $model->where('name', 'LIKE', "%{$term}%")
                ->orwhere('descriptions', 'LIKE', "%{$term}%")
                ->orwhere('price', 'LIKE', "%{$term}%")
                ->orWhere('id', $term);
        }

        if ($request->get('order_by') !== null && $request->get('order') !== 'null') {
            $order = $request->get('order') === 'ascending' ? 'asc' : 'desc';

            $model->orderBy($request->get('order_by'), $order);
        } else {
            $model->orderBy('id', 'desc');
        }

        return $model->paginate($request->get('per_page', $perPage));
    }

    /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data)
    {
        $model = $this->model->create($data);
        event(new ProductWasCreated($model, $data));
        return $model;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
        $model->update($data);
        event(new ProductWasUpdated($model, $data));
        return $model;
    }
}
