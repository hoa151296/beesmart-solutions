<?php

namespace Modules\Product\Transformers\Api;

use Illuminate\Http\Resources\Json\Resource;

class ApiProductTransformer extends Resource
{
    public function toArray($request)
    {
        $image = !empty($this->getImage()) ? $this->getImage() : '';
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'price' => (int)$this->price,
            'descriptions' => strip_tags($this->descriptions),
            'image' => ($image) ? $image->path_string : "",
        ];

        return $data;
    }
}
