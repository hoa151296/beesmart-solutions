<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Media\Support\Traits\MediaRelation;

class Product extends Model
{
	use SoftDeletes;
    use MediaRelation;

    protected $table = 'products';
    protected $fillable = [
    	'name',
    	'price',
    	'descriptions',
    ];

    public function getImage()
    {
        $image = $this->files()->where('zone', 'image')->first();

        if ($image === null) {
            return '';
        }
        return $image;
    }
}
