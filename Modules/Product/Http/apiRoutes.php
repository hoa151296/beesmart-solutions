<?php

use Illuminate\Routing\Router;
/** @var Router $router */
$router->post('upload-image', [
    'as' => 'api.uploadImage',
    'uses' => '\Modules\Product\Http\Controllers\Api\ProductController@uploadImage'
]);

$router->group(['prefix' =>'/product'], function (Router $router){
 	$router->get('list-product',[
 		'uses'=>'ProductController@listProduct',
 		'as'=>'api.product.listProduct'
 	]);

 	$router->group(['middleware' => ['api.token', 'auth.admin']], function (Router $router) {
	 	$router->get('products',[
	 		'uses'=>'ProductController@index',
	 		'as'=>'api.product.products.index',
	 		'middleware' => 'token-can:product.products.index',
	 	]);
	 	$router->post('product/{product}', [
	        'uses' => 'ProductController@find',
	        'as' => 'api.product.products.find',
	        'middleware' => 'token-can:product.products.create',
	    ]);
	 	$router->delete('product/{product}', [
	        'uses' => 'ProductController@destroy',
	        'as' => 'api.product.products.destroy',
	        'middleware' => 'token-can:product.products.destroy',
	    ]);
	    $router->post('product', [
	        'uses' => 'ProductController@store',
	        'as' => 'api.product.products.store',
	        'middleware' => 'token-can:product.products.create',
	    ]);
	    $router->post('product/{product}/edit', [
	        'uses' => 'ProductController@update',
	        'as' => 'api.product.products.update',
	        'middleware' => 'token-can:product.products.edit',
	    ]);
 	});

});