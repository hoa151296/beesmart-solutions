<?php

return [
    'list resource' => 'List products',
    'create resource' => 'Create products',
    'edit resource' => 'Edit products',
    'destroy resource' => 'Destroy products',
    'title' => [
        'products' => 'Product',
        'create product' => 'Create a product',
        'edit product' => 'Edit a product',
    ],
    'button' => [
        'create product' => 'Create a product',
    ],
    'table' => [
        'name' => 'Name',
        'price' => 'Price',
        'descriptions' => 'Descriptions',
        'image' => 'Image',
    ],
    'form' => [
        'name' => 'Name',
        'price' => 'Price',
        'descriptions' => 'Descriptions',
        'image' => 'Image',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
