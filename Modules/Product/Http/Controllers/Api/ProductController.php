<?php

namespace Modules\Product\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Transformers\Admin\ProductTransformer;
use Modules\Product\Transformers\Admin\FullProductTransformer;
use Modules\Product\Transformers\Api\ApiProductTransformer;


class ProductController extends ApiBaseController
{
    /**
     * @var ProductRepository
     */
    private $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    public function index(Request $request){
        $perPage             = !empty($request->perPage) ? $request->perPage : 10;
        $products = $this->product->serverPaginationFilteringFor($request,$perPage);
        return ProductTransformer::collection($products);
    }

    public function find(Product $product)
    {
        return new FullProductTransformer($product);
    }

    public function store(CreateProductRequest $request)
    {
        $this->product->create($request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('Create Success.'),
        ]);
    }

    public function update(Product $product, UpdateProductRequest $request)
    {
        $this->product->update($product, $request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('Update Success.'),
        ]);
    }

    public function destroy(Product $product)
    {
        $this->product->destroy($product);

        return response()->json([
            'errors' => false,
            'message' => trans('Delete Success.'),
        ]);
    }

    public function listProduct(Request $request){
        $perPage             = !empty($request->perPage) ? $request->perPage : 10;
        $products = $this->product->serverPaginationFilteringFor($request,$perPage);
        $data = ApiProductTransformer::collection($products);
        return $this->respondWithPagination($products,$data);
    }
}
